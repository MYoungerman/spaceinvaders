//
//  Alien.hpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 11/02/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//

#ifndef Alien_hpp
#define Alien_hpp

#include <GLUT/glut.h>
#include "Entity.hpp"
#include "Missile.hpp"
#include <vector>
#include <stdlib.h>  

class Alien : public Entity {
    
public:
    Alien(float x,float y);
    int ts;
    bool flip;
    std::vector  <Missile*> missiles;
    static float dir;
    void draw();
    void update();
    
};
 

#endif /* Alien_hpp */
