//
//  Entity.hpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 13/02/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//

#ifndef Entity_hpp
#define Entity_hpp

class Entity{

public:
    
    float xpos, ypos;
    int size;
    int alive;
    virtual void update();
    virtual void draw();
    
    
};

#endif /* Entity_hpp */
