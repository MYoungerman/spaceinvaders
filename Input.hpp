//
//  Input.hpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 06/02/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//

#ifndef Input_hpp
#define Input_hpp

#include <GLUT/glut.h>

class Input{
    
    public:
    static bool upKey;
    static bool downKey;
    static bool leftKey;
    static bool rightKey;
    static bool sbKey;
    static float x;
    static float rotate;
    static void specialKeys(int key, int x, int y);
    static void specialUpFunc(int key, int x, int y);
    static void KeyboardDown(unsigned char key, int x, int y);
    static void KeyboardUp(unsigned char key, int x, int y);
    
};

#endif /* Input_hpp */
