//
//  Alien.cpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 11/02/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//

#include "Alien.hpp"


Alien::Alien(float x,float y)
{
    xpos=x;
    ypos=y;
    ts=0;
    flip=0;
    alive=1;
}

void Alien::draw()
{
    glColor3f(1.0, 1.0, 1.0);

    glLoadIdentity();
    
    glTranslatef(xpos, ypos, 0);
    glScalef(0.5, 0.5, 0.5);
    

    glTranslatef(-0.18, 0.0, 0);
    if(flip)
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    if (flip)
    glutSolidCube(0.02);
    

    glTranslatef(-0.18, -0.02, 0);
    if(flip)
    glutSolidCube(0.02);
    glTranslatef(0.02, 0.0, 0);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.04, 0, 0);
    if(flip)
    glutSolidCube(0.02);

    
    
    
    glTranslatef(-0.18, -0.02, 0);
    if (flip)
    glutSolidCube(0.02);
    glTranslatef(0.04, 0.0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.10, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.04, 0, 0);
    if(flip)
    glutSolidCube(0.02);

    
    glTranslatef(-0.18, -0.02, 0);
    if(flip)
    glutSolidCube(0.02);
    glTranslatef(0.04, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.10, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.04, 0, 0);
    if(flip)
    glutSolidCube(0.02);
    
    
    //bottom line
    if(flip)
    {
        glTranslatef(-0.12, -0.02, 0);
        glutSolidCube(0.02);
        glTranslatef(0.06, 0, 0);
        glutSolidCube(0.02);
        glTranslatef(-0.10, 0.10, 0);
    }
    else
    {
        glTranslatef(-0.16, -0.02, 0);
        glutSolidCube(0.02);
        glTranslatef(0.14, 0, 0);
        glutSolidCube(0.02);
        glTranslatef(-0.14, 0.10, 0);
    }
    
    if(!flip)
    {
         glTranslatef(-0.02, 0.0, 0);
         glutSolidCube(0.02);
         glTranslatef(0.02, 0.0, 0);
    }
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.04, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.04, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    if(!flip)
    {
        glTranslatef(0.02, 0.0, 0);
        glutSolidCube(0.02);
        glTranslatef(-0.02, 0.0, 0);
    }
    
    
    glTranslatef(-0.12, 0.02, 0);
    if(!flip)
    {
        glTranslatef(-0.04, 0.0, 0);
        glutSolidCube(0.02);
        glTranslatef(0.04, 0.0, 0);
    }
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    glTranslatef(0.02, 0, 0);
    glutSolidCube(0.02);
    if(!flip)
    {
        glTranslatef(0.04, 0.0, 0);
        glutSolidCube(0.02);
        glTranslatef(-0.04, 0.0, 0);
    }
    
    
    glTranslatef(-0.08, 0.02, 0);
    if(!flip)
    {
        glTranslatef(-0.06, 0.0, 0);
        glutSolidCube(0.02);
        glTranslatef(0.06, 0.0, 0);
    }
    glutSolidCube(0.02);
    glTranslatef(0.06, 0, 0);
    glutSolidCube(0.02);
    if(!flip)
    {
        glTranslatef(0.06, 0.0, 0);
        glutSolidCube(0.02);
        glTranslatef(-0.06, 0.0, 0);
    }
    
    //top line
    glTranslatef(-0.08, 0.02, 0);
    glutSolidCube(0.02);
    glTranslatef(0.1, 0, 0);
    glutSolidCube(0.02);

}


void Alien::update(){
    
    
    if( glutGet(GLUT_ELAPSED_TIME) > ts+700 )
    {
        flip=(!flip);
        xpos+=Alien::dir;
        ts=glutGet(GLUT_ELAPSED_TIME);
    }
}

